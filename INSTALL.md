Building Miracle
================

See doc/build-*.md for instructions on building the various
elements of the Miracle Core reference implementation of Miracle.
